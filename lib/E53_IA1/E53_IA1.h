#include <BH1750.h>
#include <Wire.h>
#include "SHT31.h"

//自定义IIC的引脚
#define I2C_SDA_PIN 4
#define I2C_SCL_PIN 5

// 定义 E53_IA1 模块的 I2C 地址
#define SHT31_ADDRESS   0x44
SHT31 sht30;
BH1750 lightMeter(0x23);

uint32_t start;
uint32_t stop;

//E53_IA1的属性名称
const char* device1 = "light";
const char* device2 = "temp";
const char* device3 = "hum";

//E53_IA1的温湿度和光照数据，结构体类型
struct Data{
    float temp;
    float hum;
    float light;
};

Data ReadData();
