#include "E53_IA1.h"

/*函数名称: ReadData
*说明: 读取E53_IA1的温度、湿度和光照数据
*参数: 无
*返回值:包含三个数据的结构体Data
*/
Data ReadData(){
    //获取光照强度
    float lux = lightMeter.readLightLevel();
    //获取温湿度
    sht30.read();     
    float temperature = sht30.getTemperature();
    float humidity = sht30.getHumidity();
    Data data;
    data.hum = humidity;
    data.temp = temperature;
    data.light = lux;
    return data;
}