#ifndef L610_H
#define L610_H

#include <Arduino.h>
//定义esp32与L610相连的引脚
#define PIN_L610_RX 18
#define PIN_L610_TX 19

void SendCmdToL610(String data,const char* res);  //发送消息给L610
void L610_Attach(bool reset);      //初始化L610，激活4G网络

/*-------------------------MQTT部分-------------------------*/  
#ifdef MQTT

    //配置mqtt账户和设备号
    const char* mqttServer_ip = "www.aiotcomm.com.cn";  //域名地址
    // const char* mqttServer_ip = "39.108.76.174";     //ip地址
    const char* mqttServer_Port = "11883";              //  MQTT 端口号

    //设置mqtt设备名和访问密码
    const char* deviceid = "1";                   //设备唯一标识号
    const char* clientid = "esp32_4g_mqtt_ht";    //本设备的名称
    const char* mqttusername = "l610";            //使用mqtt协议连接云平台时所用的用户名
    const char* mqttpwd = "l610";                 //使用mqtt协议连接云平台时所用的密码

    //云平台的mqtt产品名称、设备名称、设备属性
    const char* productname = "esp32-mqtt";
    const char* devicename = "esp32-4G";

    //设置mqtt主题topic
    const char* transdatatopic = "v1/devices/me/telemetry";  //	网关/直连设备遥测
    const char* serverpubtopic = "v1/devices/me/rpc/request/+"; //服务端命令下发到设备端

    //通过L610与云平台建立MQTT连接
    void MqttConnect(const char* device_id, const char* user_name,const char* password,const char* ClientIDStr, const char* mqttServer_ip,const char* mqttServer_Port,
    const char* CleansessionFlag ,const char* Keepalive_Time); 
    //定义函数实现publish和subscribe功能
    void L610_MQTTPub(const char* device_id, const char* topic,const int QoS,const int remain_flag,String data);  
    void L610_MQTTSub(const char* device_id, const char* topic,const int QoS);

#endif

/*-------------------------TCP部分-------------------------*/  
#ifdef TCP 

    //配置tcp访问信息
    const char* socket_id = "1";  //网络接入标识符
    const char* access_token = "L610tcp";  //TCP鉴权令牌
    // const char* server_ip = "39.108.76.174";  //云平台ip地址
    const char* server_ip = "www.aiotcomm.com.cn";  //云平台域名地址
    const char* server_port = "18088";    //云平台端口

    void TCPConnect(const char* socket_id,const char* server_ip,const char* server_port,const char* type);  //通过L610连接TCP服务器
    void SendDataToTCPServer(const char* socket_id,String data);    //发送消息到tcp或udp服务器
#endif


#endif

